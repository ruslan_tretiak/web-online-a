<!DOCTYPE html>
<html lang="ru">

<head>

	<meta charset="utf-8">

	<title>Вы почти в списке участниц</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:image" content="img/favicon/apple-touch-icon.png">
	<link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="img/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-touch-icon-114x114.png">
        <link rel="stylesheet" type="text/css" href="css/timer.css">

	<!-- Chrome, Firefox OS and Opera -->
	<meta name="theme-color" content="#000">
	<!-- Windows Phone -->
	<meta name="msapplication-navbutton-color" content="#000">
	<!-- iOS Safari -->
	<meta name="apple-mobile-web-app-status-bar-style" content="#000">

	<style>body { opacity: 0; overflow-x: hidden; } html { background-color: #fff; }</style>


<!-- Facebook Pixel Code Zhenya-->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1551016298300708');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1551016298300708&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Facebook Pixel Code LTD-->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '249955575612985');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=249955575612985&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '139975586551334');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=139975586551334&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



<!-- Facebook Pixel Code Samoylov -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1523228791279663');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1523228791279663&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code FR -->
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '249955575612985');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=249955575612985&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter25611482 = new Ya.Metrika({
                    id:25611482,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25611482" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53025216-1', 'auto');
  ga('send', 'pageview');

</script>
<style>
.sc_1 .text ul li.c1:before {
    content: "1.";
        position: absolute;
    font-family: SFUITextMedium;
    margin-left: -20px;
}
.sc_1 .text ul li.c2:before {
    content: "2.";
        position: absolute;
    font-family: SFUITextMedium;
    margin-left: -20px;
}
.sc_1 .text ul li.c3:before {
    content: "3." !important;
        position: absolute;
    font-family: SFUITextMedium;
    margin-left: -20px;
}
</style>

<!-- Global site tag (gtag.js) - Google AdWords: 797251820 -->

<script async src="https://www.googletagmanager.com/gtag/js?id=AW-797251820"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'AW-797251820');
</script>

<!-- Event snippet for Put_main conversion page -->
<script>
gtag('event', 'conversion', {'send_to': 'AW-797251820/cdcSCPeR74UBEOyxlPwC'});
</script>

<script>(function(w,d,k){w['r7k12']=w['r7k12']||[];var s=d.createElement('script');s.async=1;s.src='https://r7k12.com/scripts/'+k+'/counter.js';s.type='application/javascript';d.head.appendChild(s);})(window,document,'9191c72e3857c4197d3c26434b2f54f0');r7k12.push({hit:'pageview'});</script>

</head>

<body>

<script charset="UTF-8" src="//cdn.sendpulse.com/9dae6d62c816560a842268bde2cd317d/js/push/749ca6c11473fc709b05ded0c8df3b15_1.js" async></script>
<script type="text/javascript">
   (function (atm, doIt) {
       // atm.order_match = ‘546’; // OPTIONAL: Order MATCH
       // atm.order_value = ‘19.99’; // OPTIONAL: Order value
       // atm.product_id = ‘product-id’; // OPTIONAL: Product name or product ID

       // Do not modify any of the code below.
       atm.client_id = "c8c5d9e6805cb9c1056f3965704c65a8"; // CLIENT: Yaroslav-samoylov.com
       doIt(atm);
   })({}, function(b){var a=document.createElement("script"),c=document.getElementsByTagName("script")[0];a.type="text/javascript";a.async=!0;a.src="https://static-trackers.adtarget.me/javascripts/pixel.min.js";a.id="GIHhtQfW-atm-pixel";a["data-pixel"]=b.client_id;a["uniq"]=b.uniq;a.settings_obj=b;c.parentNode.insertBefore(a,c)});

   //  After the pixel above is loaded, you can use custom AJAX calls to register custom events (like button clicks, form fillouts and etc. )
   //__AtmUrls = window.__AtmUrls || [];
   //__AtmUrls.push(‘add-to-cart’);
</script>
<style>
.desc{
	display: none;
}
.mob{
	display: block;
	margin: 3px;
}
@media screen and (max-width: 480px){
	.desc{
		display: block;
	}
	.mob{
		display: none;
		
	}
}
.module.visible {
    text-align: center;
    background: #017bfe;
    color: #fff;
    /*text-transform: uppercase;*/
}
.module.visible a {
    background: #fff;
    margin: 10px;
    padding: 0px 10px;
    display: inline-block;
    color: #027afe;
    border-radius: 16px;
    box-shadow: 0px 0px 15px rgba(255, 255, 255, 0.64);
}
.module.visible a:hover {
    box-shadow: 0px 0px 5px rgba(255, 255, 255, 0.64);
}
.sc_5 .wrapper img {
    max-width: 380px !important;
    max-height: 300px !important;
    width: initial !important;
    height: inherit !important;
}
.sc_5 {
    margin-top: 33px;
    margin-bottom: 60px;
}

.sc_3 .options .option_2 .free {
    margin-bottom: 32px !important;
}
.indicator {
    display: table;
    width: 30%;
    height: 40px;
    border-radius: 20px;
    border: 2px solid rgb(77, 102, 164);
    overflow: hidden;
}
.indicator div:first-child {
    color: rgb(236, 239, 241);
    background-color: rgb(77, 102, 164);
}
.indicator div {
    display: table-cell;
    vertical-align: middle;
    text-align: center;
    width: 50%;
    height: 100%;
}
.sc_1 .text p {
    margin: 34px 0 10px 30px !important;
    font-family: SFUITextMedium;
    width: 410px;
    font-weight: 500;
}
@media only screen and (max-width: 480px){
.sc_1 h2 {
    font-size: 17px;
    padding-top: 20px;
    line-height: 20px;
    margin-bottom: 30px;
}
footer p:nth-of-type(1) {
    padding: 0px 0 0px !important;
}
.sc_1 .text p {
margin: 20px 0 20px 0px !important;
    font-family: SFUITextMedium;
    width: 410px;
    font-weight: 500;
}
}
.sc_1 .text ul li {
    line-height: 1.28;
}

footer p:nth-of-type(1) {
    padding: 0px 10% 0 !important;
}
</style>

	<section class="sc_1">
		<div class="wrapper">
			<h2><?php echo $_GET['name'] ? $_GET['name'] : "Дорогая " ?>, Вы почти в списке участниц</h2>
			<center>
                            <div class="indicator">
                                <div>50%</div>
                                <div></div>
                            </div>
                            <h3 class="text-center">Остался один шаг</h3>
            </center>
            <center>
            <div class="wepster-hash-xiv5dv"></div>
            </center>
			<div class="video">
				<iframe width="100%" height="100%" src="https://www.youtube.com/embed/1Osr_ZEkcJc?autoplay=1&rel=0&controls=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="text">
				<p>Чтобы точно получить ссылку на<br/> участие в бесплатном онлайн-курсе<br/> 12-13-14-15-16 ноября (пн-вт-ср-чт-пт) "Мужчина: честная инструкция" и  подарок:</p>
				<ul>
<!-- 					<li class="c1">Прочитайте эту страницу до конца</li> -->
					<li class="c1" style="margin-bottom: 20px;">Прямо сейчас перейдите на Ваш email и откройте письмо<br> от Ярослава Самойлова с темой <br><b style="    font-family: SFUITextMedium;">✅ [Вы в списке] осталась минута...</b><br><span style="margin-top: 10px; display: block; color: #fa1869; font-size: 14px;">(По ошибке могло попасть в папку спам или промоакции).</span></li>
					<!-- <li class="c3">Узнайте, кто создает счасте</li> -->
          <li class="c2" style="position: relative;">Успейте забрать в письме Ваш подарок мини-книгу<br> "5 вопросов, чтобы определить, Достойный ли мужчина перед Вами".</li>
				</ul>
			</div>
		</div>
	</section>

	<section class="sc_4">
		<div class="wrapper">
			<!-- <h2>Как гарантированно попасть на онлайн-курс бесплатно?</h2> -->
			<ul>
				<!-- <li>Прямо сейчас перейдите на почту, указанную при регистрации.</li> -->
				<!-- <li>Найдите письмо от Ярослава Самойлова с темой <span>✅ [Вы в списке!] осталась минута...</span></li> -->
				<!-- <li>Заберите в письме Ваш подарок мини-книгу.</li> -->
			</ul>
		</div>
	</section>
<style>
.module .internal-module {
    display: block;
}
li.c2:after {
    position: absolute;
    bottom: -115px;
    left: 10px;
    content: " ";
    z-index: 9999;
    display: block;
    width: 300px;
    transform: scale(-1, 1) rotate(40deg);
    height: 60px;
    background-repeat: no-repeat;
    background-position: 50% 50%;
    background-image: url(../img/arrow.png);
}
@media only screen and (max-width: 480px){
  li.c2:after {display: none}
}
</style>
	<section class="sc_5" style="margin-top: 15px;">
		<div class="wrapper">
                    <div class="module">
<!--                         <div class="internal-module">
                            <div class="module-with-timer">
                                <div class="timer">
                                    <div class="timer-main-part">
                                        <div class="days"><span>00</span></div>
                                        <div class="hours"><span>00</span></div>  
                                        <div class="minutes"><span>00</span></div>
                                        <div class="seconds"><span>00</span></div> 
                                    </div> 
                                    <div class="timer-marks">
                                        <span>дней</span>
                                        <span>часов</span>
                                        <span>минут</span>
                                        <span>секунд</span>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="internal-module">
                            <img src="https://yaroslav-samoylov.com/pages/yaroslav/fev-web-online/img/book.png" alt="10_words">
                        </div>
                    </div>
		</div>
	</section>

	
	<style type="text/css">
   .timer-marks span {
    font-size: 11px;
        color: #4d65a4;
  } 
  .timer-main-part div {
    height: 40px;
        background-image: initial !important;
  }
  </style>
	<footer>
		<div class="wrapper">
      <div class="footer-center-text" style="padding-top: 20px;">
          <p>Россия: <span itemprop="telephone">+7 (499) 322 87 26</span></p>
          <p>Украина: <span itemprop="telephone">+38 (067) 329 88 38</span></p>
          <p>Казахстан: <span itemprop="telephone">+77 (800) 080 49 77</span></p>
        </div>
			<p>© 2014-2018.<br> Все права защищены. Любое копирование материалов разрешено только с согласия правообладателей.</p>
			<p>По всем вопросам: info@yaroslav-samoylov.com</p>
			<div class="links">
				<a href="http://yaroslav-samoylov.com/politika-konfidentsialnosti.html/">Политика конфиденциальности</a>
				<a href="http://yaroslav-samoylov.com/otkaz-ot-otvetstvennosti.html/">Отказ от ответственности</a>
				<a href="http://yaroslav-samoylov.com/soglasie-s-rassylkoj.html/">Согласие с рассылкой</a>
				<a href="http://yaroslav-samoylov.com/garantiya-vozvrata.html/">Гарантия возврата</a>
				<a href="http://yaroslav-samoylov.com/publichnyj-dogovor-oferta.html/">Публичный договор <br>(оферта)</a>
			</div>
		</div>
	</footer>

	<link rel="stylesheet" href="css/main.min.css">
	<script src="js/scripts.min.js"></script>
<!-- Конверсия Adwords - Если что-то не работает - удаляем ее
Google Code for &#1055;&#1091;&#1090;&#1100; 19 - &#1042;&#1077;&#1073; 2 - &#1050;&#1086;&#1085;&#1074;&#1077;&#1088;&#1089;&#1080;&#1103; &#1095;&#1077;&#1088;&#1077;&#1079; &#1090;&#1077;&#1075; Conversion Page -->

<!-- Google Code for &#1055;&#1091;&#1090;&#1100; 20 - &#1042;&#1077;&#1073; 3 Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 935894673;
var google_conversion_label = "jMD8CIWpy3gQkb2ivgM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/935894673/?label=jMD8CIWpy3gQkb2ivgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Google Code for &#1055;&#1091;&#1090;&#1100; 20 - &#1042;&#1077;&#1073; 3 Conversion Page -->




<script src="js/basic-functions.js"></script>
<script src="js/cookie-functions.js"></script>
<script src="js/timer.js?v=1"></script>
<!-- leeloo init code -->
<script>
    window.LEELOO = function(){
        window.LEELOO_INIT = { id: '5b8567887e996d001e24361d' };
        var js = document.createElement('script');
        js.src = 'https://app.leeloo.ai/init.js';
        document.getElementsByTagName('head')[0].appendChild(js);
    }; LEELOO();
</script>
<!-- end leeloo init code -->
<script>
window.onload = function() {
    "use strict";
//    var timerObject = new Timer();
//    timerObject.initilize(document.getElementsByClassName("timer")[0]);
//    timerObject.setStaticTimer();
};
</script>

</body>
</html>
