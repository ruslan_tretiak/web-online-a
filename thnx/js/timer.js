function Timer() {
    "use strict";
    function countTime() {
        var resetsAmount = 0;     //Переменная указывает количество "обнулений" таймера;
        var shareDuration = 24;    //Переменная указывает длительность акции;
        var deviation = 0;          //Переменная указывает, сколько миллисекунд осталось до "обнуления" таймера;
        var variable = 0;           //Дополнительная переменная, используется для визуализации работы таймера;
        var initialDate = "2018-02-08";
        var endingDate = "";
        var currentDate = new Date();
        var initialDay = initialDate.split("-")[2];
        var initialMonth = initialDate.split("-")[1];
        var initialYear = initialDate.split("-")[0];
        //Количество "обнулений" таймера (общее количество дней, которые прошли с момента начальной даты до текущей, делится на длительность акции, получается
        //количество полных "обнулений" таймера до этого момента);
        resetsAmount = Math.floor(calculateDaysNumber(initialDate + "T00:00:00Z") / shareDuration);
        //Дата следующего "обнуления" (программа автоматически вычисляет дату следующего "обнуления" путем прибавления к начальной дате результата умножения
        //[количества полных "обнулений" таймера + 1] * длительность акции);
        endingDate = new Date(initialYear, initialMonth - 1, parseInt(initialDay) + (resetsAmount + 1) * shareDuration);
        deviation = endingDate - currentDate;
        if (deviation < 0) {
            endingDate = new Date(initialYear, initialMonth - 1, parseInt(initialDay) + (resetsAmount + 2) * shareDuration);
            deviation = endingDate - currentDate;
        }
        //Количество дней, которое осталось до "обнуления" таймера (в сутках "86400000" милисекунд);
        variable = parseInt(deviation / 86400000);
        daysModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
        //Количество часов, которое осталось до "обнуления" таймера (в часе "3600000" милисекунд);
        variable = parseInt(deviation / 3600000) % 24;
        hoursModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
        //Количество минут, которое осталось до "обнуления" таймера (в минуте "60000" милисекунд);
        variable = parseInt(deviation / 60000) % 60;
        minutesModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
        //Количество минут, которое осталось до "обнуления" таймера (в секунде "1000" милисекунд);
        variable = parseInt(deviation / 1000) % 60;
        secondsModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
    }
    function addZero(variable) {
        //Функция определяет, нужно ли добавлять "0" перед полученным числом;
        if (variable < 10) variable = "0" + variable.toString();
        else variable = variable.toString();
        return variable;
    }
    function calculateDaysNumber(initialDate) {
        //Функция вычисляет количество прошедших дней между текущей датой и датой инициализации таймера;
        return (new Date() - new Date(initialDate)) / 86400000;
    }
    var timerClassName = "timer";
    var daysModuleClassName = "days";
    var hoursModuleClassName = "hours";
    var minutesModuleClassName = "minutes";
    var secondsModuleClassName = "seconds";
    var timer = new Object();
    var daysModule = new Object();
    var hoursModule = new Object();
    var minutesModule = new Object();
    var secondsModule = new Object();
    this.initilize = function(element) {
        var additoryObject = new Object();
        if (testClassName(element, timerClassName)) {
            timer = element;
            additoryObject = selectElementByClassName(daysModuleClassName, timer);
            if (additoryObject.status) {
                daysModule = additoryObject.element;
                additoryObject = selectElementByClassName(hoursModuleClassName, timer);
                if (additoryObject.status) {
                    hoursModule = additoryObject.element;
                    additoryObject = selectElementByClassName(minutesModuleClassName, timer);
                    if (additoryObject.status) {
                        minutesModule = additoryObject.element;
                        additoryObject = selectElementByClassName(secondsModuleClassName, timer);
                        if (additoryObject.status) {
                            secondsModule = additoryObject.element;
                        } else notify("Не был найден DOM-элемент с классом '" + secondsModuleClassName + "';");
                    } else notify("Не был найден DOM-элемент с классом '" + minutesModuleClassName + "';");
                } else notify("Не был найден DOM-элемент с классом '" + hoursModuleClassName + "';");
            } else notify("Не был найден DOM-элемент с классом '" + daysModuleClassName + "';");
        } else notify("DOM-элемент должен содержать класс '" + timerClassName + "';");
    };
    this.setDynamicTimer = function() {
        window.setInterval(function() {
            countTime();
        }.bind(this), 1000);
    };
    this.setStaticTimer = function() {
        var indicatorTitle = "timer";
        var currentTime = Date.now();
        var duration = 300000;
        var internalCounter = 0;
        var interval = 0;
        try {
            if (getCookie(indicatorTitle)) duration = parseInt(getCookie(indicatorTitle)) - currentTime;
            else setCookie(indicatorTitle, currentTime + duration, {"expires": new Date(currentTime + duration)});
            interval = window.setInterval(function() {
                var variable = 0;
                variable = parseInt((duration - internalCounter) / 86400000);
                daysModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
                variable = parseInt((duration - internalCounter) / 3600000) % 24;
                hoursModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
                variable = parseInt((duration - internalCounter) / 60000) % 60;
                minutesModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
                variable = parseInt((duration - internalCounter) / 1000) % 60;
                secondsModule.getElementsByTagName('span')[0].innerHTML = addZero(variable);
                internalCounter += 1000;
                if (internalCounter >= duration) window.clearInterval(interval);
            }.bind(this), 1000);
        } catch (error) {
            if (error instanceof  ReferenceError) {
                notify("Не подключен скрипт 'cookie-functions.js';");
            }
        }
     };
}