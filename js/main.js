$(document).ready(function () {
	
	$(".button-ok").click(function () {
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#elementId").offset().top
		}, 2000);
	});

	$(".click-menu-element").click(function () {
		$([document.documentElement, document.body]).animate({
			scrollTop: $("#elementId").offset().top
		}, 2000);
	});

	$("#form-a-privacy").click(function() {
		blockPrivacy(this)
	});
	$("#form-a-privacy-footer").click(function() {
		blockPrivacy(this)
	});


	function blockPrivacy($this) {
		var className = $($this).attr('class'),
			requestButton = $('.request-caller');
		if('privacy' == className){
			$('#form-a-privacy').prop('checked', false);
			$($this).removeClass();
			$($this).addClass('privacy-change');
			$(requestButton).prop('disabled', true);
			$(requestButton).css('background-color', '#ff6055');

		}
		if('privacy-change' == className){
			$('#form-a-privacy').prop('checked', true);
			$($this).removeClass();
			$($this).addClass('privacy');
			$(requestButton).prop('disabled', false);
			$(requestButton).css('background-color', '#e30000');
		}
	}

	$('.submit-footer').click( function() {
		loadForm('ajaxform-1')
	})

	$('.submit-header').click( function() {
		loadForm('ajaxform')
	})
	
	function loadForm(id) {
		var form = $('#'+id);
		var data = form.serializeArray();
		var result = true;
		$.each($(form).find('input'), function (key, input) {
			if('email' === $(input).attr('name') || 'phone' === $(input).attr('name')) {
				if ('' === $(input).val()) {
					$(input).css("border", "1px solid red");
					result = false;
				} else {
					$(input).css("border", "1px solid #ebebeb");
				}
			}
		});
		if(result) {
			makeApiCall(data);
			postAjax(data)
		}
	}

	function postAjax(data) {
		$.ajax({
			type: 'POST',
			url: 'https://air2.yaroslav-samoylov.com/lead/zoho/',
			dataType: 'json',
			data: data,
			beforeSend: function (data) {
				startLoader('Идет отправка данных...');
			},
			success: function (data) {
				window.location.href = data['redirect'];
			},
			error: function (data) {
				stopLoader();
				alert('Возникла ошибка, заполните, пожалуйста, форму корректными данными и попробуйте еще раз!');
			}
		})
	}

	function startLoader(text){
		var loader = $('.loaders');
		$(loader).find('.preloader').append('\n' +
			'        <img style="margin-left: 10%;" src="./img/ww.svg">' +
			'<div class="loader_text">'+text+'</div>');
		$(loader).addClass('preloader-wrapper');
	}

	function stopLoader(){
		var loader = $('.loaders');
		$('.loaders').removeClass('loaders preloader-wrapper').addClass('loaders ');
		$(loader).find('.preloader').empty();
	}
	
	$.get("https://api.2ip.ua/geo.json?key=b8b8e66abf73f451", function (response) {
		var city = response.region;
		var country = response.country_code;
		var city_rus = response.city_rus;
		var country_rus = response.country_rus;

		$('#geo').val(country_rus+'.');
		$('#geo-1').val(country_rus+'.');
		$('#city').val(city_rus+'.');
		$('#city-1').val(city_rus+'.');

		$('#phone').click(function(){
			chengPhone(country, 'phone')
		})

		$('#phone-1').click(function(){
			chengPhone(country, 'phone-1')
		})
	}, "json");
	
	function chengPhone(country, idPhone) {
		if (country == 'UA') {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+380');
			}
		}
		else if (country == "RU" || country == "KZ") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+7');
			}
		}
		else if (country == "BY") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+375');
			}
		}
		else if (country == "TR") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+90');
			}
		}
		else if (country == "AZ") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+994');
			}
		}
		else if (country == "KG") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+996');
			}
		}
		else if (country == "MD") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+373');
			}
		}
		else if (country == "LV") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+371');
			}
		}
		else if (country == "GE") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+995');
			}
		}
		else if (country == "UZ") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+998');
			}
		}
		else if (country == "PL") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+48');
			}
		}
		else if (country == "EE") {
			if ($('#'+idPhone).val() == '') {
				$('#'+idPhone).val('+372');
			}
		}
		else {
			$('#'+idPhone).val('+');
		}
	}



	timer("Oct 30, 2018 19:00:00");

	function timer(time) {
		var days, hours, minutes, seconds;
		var countDownDate = new Date(time).getTime();

		var x = setInterval(() => {
			var now = new Date().getTime();
			var distance = countDownDate - now;

			$('#header-ribbon').fadeIn(300);

			if (distance > 0) {
				days = Math.floor(distance / (1000 * 60 * 60 * 24));
				hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
				minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
				seconds = Math.floor((distance % (1000 * 60)) / 1000);
			} else {
				$('#header-ribbon').remove();
				// $('#air-livemodal').modal();
				clearInterval(x);
			}

			$('[data-hours]').text(hours);
			$('[data-minutes]').text(minutes);
			$('[data-seconds]').text(seconds);
		}, 1000);
	}





	$('head').append('\
<style>\
#ajaxform input[type=button]:hover{\
    background: rgba(210, 41, 32, 0.8) !important;\
    color: #fff !important;\
    border: 0px !important;\
    cursor: pointer;\
}\
</style>');

	$('head').append('\
<style>\
input[type="text"], input[type="email"], input[type="tel"] {\
    margin: 5px 0px;\
    background-color: rgb(242, 242, 242);\
    font-family: "__SF UI Text_5";\
    color: rgb(80, 80, 80);\
    min-height: 19px;\
    font-size: 15px;\
    font-weight: 300;\
    width: 100%;\
    padding: 11px 6px 6px 16px;\
    border-radius: 6px;\
    border: 1px solid rgb(195, 195, 195);\
}\
</style>');

	$('head').append('\
<style>\
input[type="button"] {\
    margin: 20px 0px 5px;\
    background-color: rgb(210, 41, 32);\
    font-family: "__SF UI Text_5";\
    color: rgb(255, 255, 255);\
    min-height: 19px;\
    font-size: 24px;\
    font-weight: 300;\
    width: 280px;\
    padding: 14px 40px;\
    border-radius: 28px;\
    border: 0px;\
    text-align: center;\
    text-transform: uppercase;\
}\
</style>');

	$('head').append('\
<style>\
#ajaxform input[type=button]:hover{\
    background: rgba(210, 41, 32, 0.8) !important;\
    color: #fff !important;\
    border: 0px !important;\
    cursor: pointer;\
}\
</style>');

	$('head').append('\
<style>\
input[type="text"], input[type="email"], input[type="tel"] {\
    margin: 5px 0px;\
    background-color: rgb(242, 242, 242);\
    font-family: "__SF UI Text_5";\
    color: rgb(80, 80, 80);\
    min-height: 19px;\
    font-size: 16px;\
    font-weight: 300;\
    width: 100%;\
    padding: 12px 5px 11px 16px;\
    border-radius: 6px;\
    border: 1px solid rgb(195, 195, 195);\
}\
</style>');

	$('head').append('\
<style>\
input[type="button"] {\
    margin: 20px 0px 5px;\
    background-color: rgb(210, 41, 32);\
    font-family: "__SF UI Text_5";\
    min-height: 19px;\
    font-size: 24px;\
    font-weight: 300;\
    width: 280px;\
    padding: 14px 40px;\
    border-radius: 28px;\
    border: 0px;\
    text-align: center;\
    text-transform: uppercase;\
}\
</style>');



	var $items = $('#vtab>ul>li');
	$items.mouseover(function() {
		$items.removeClass('selected');
		$(this).addClass('selected');

		var index = $items.index($(this));
		$('#vtab>div').hide().eq(index).show();
	}).eq(0).mouseover();
	
	
});