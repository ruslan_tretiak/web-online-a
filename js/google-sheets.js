
async function makeApiCall(data) {
	var params = {
		spreadsheetId: '14eNVhwPsMAUTXRkWUwtDqu3aUdcVOEBcnniLSVZlq98',
		range: 'Аркуш1',
		valueInputOption: 'RAW',
		insertDataOption: 'INSERT_ROWS',
	};
	var newData = [];
	$.each(data, function (key, input) {
		newData.push(input.value)
	})

	var now = new Date();
	var date = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
	newData.push(date)

	var valueRangeBody = {
		"values": [
			newData
		],
	};

	var request = gapi.client.sheets.spreadsheets.values.append(params, valueRangeBody);
	request.then(function(response) {
	}, function(reason) {
		console.error('error: ' + reason.result.error.message);
	});
}

function initClient() {
	var API_KEY = 'AIzaSyDLdHEPDDLuSZgljh74pjgXC8yixiBD2Tk';
	var CLIENT_ID = '416845358524-4bkju9codvs2a9icg4on2gc39mouan7s.apps.googleusercontent.com';
	var SCOPE = 'https://www.googleapis.com/auth/spreadsheets';

	gapi.client.init({
		'apiKey': API_KEY,
		'clientId': CLIENT_ID,
		'scope': SCOPE,
		'discoveryDocs': ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
	}).then(function() {
		gapi.auth2.getAuthInstance().isSignedIn.listen(updateSignInStatus);
		// updateSignInStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
	});
}

function handleClientLoad() {
	gapi.load('client:auth2', initClient);
}

function updateSignInStatus(isSignedIn) {
	console.log(isSignedIn);
	// if (isSignedIn) {
	//     makeApiCall();
	// }
}

function handleSignInClick(event) {
	gapi.auth2.getAuthInstance().signIn();
}

function handleSignOutClick(event) {
	gapi.auth2.getAuthInstance().signOut();
}