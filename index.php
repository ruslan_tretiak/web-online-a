<?php
//  $query = $_SERVER['QUERY_STRING'];
//  $host = $_SERVER['HTTP_HOST'];
//  $url = "https://static.yaroslav-samoylov.com/pages/feb-web-online-a/" . ($query ? '?' . $query : '');
//  header("Location: $url",true,301);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <link rel=icon href=favicon.ico>
    <meta property=og:title content="Мужчина: честная инструкция по применению. Без драм и манипуляций.">
    <meta property=og:description
          content="ПРИСОЕДИНЯЙСЯ. Бесплатный онлайн мастер-класс Ярослава Самойлова. Психология отношений глазами мужчины. Как привлекать достойных мужчин и строить счастливые отношения.">
    <meta property=og:url content=https://master-klassy.yaroslav-samoylov.com/ >
    <meta property=og:image content=https://master-klassy.yaroslav-samoylov.com/vk-fb.jpg>
    <meta name=title content="Мужчина: честная инструкция по применению. Без драм и манипуляций.">
    <meta name=description
          content="ПРИСОЕДИНЯЙСЯ. Бесплатный онлайн мастер-класс Ярослава Самойлова. Психология отношений глазами мужчины. Как привлекать достойных мужчин и строить счастливые отношения.">
    <title>Мужчина: честная инструкция по применению. Без драм и манипуляций. | Тренинг Ярослава Самойлова</title>

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel=stylesheet type="text/css" href="css/bootstrap.min.css" rel=stylesheet>
    <link rel="stylesheet" type="text/css" href="style.css?v=1.0.3">
    <link rel="stylesheet" type="text/css" href="css/validation.css">
    <link rel="stylesheet" type="text/css" href="css/spinner.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel=canonical href="https://yaroslav-samoylov.com/pages/yaroslav/dec-web-online-n/"/>
    <script async defer>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WMT32VV');</script>
    <script async defer>!function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '278927249614169');
        fbq('track', 'PageView');</script>
    <script async defer>(function (d, w, c) {
            (w[c] = w[c] || []).push(function () {
                try {
                    w.yaCounter25611482 = new Ya.Metrika({
                        id: 25611482,
                        clickmap: true,
                        trackLinks: true,
                        accurateTrackBounce: true,
                        webvisor: true
                    });
                } catch (e) {
                }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () {
                    n.parentNode.insertBefore(s, n);
                };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://d31j93rd8oukbv.cloudfront.net/metrika/watch_ua.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else {
                f();
            }
        })(document, window, "yandex_metrika_callbacks");</script>
</head>

<?php $product = 935; ?>

<body>
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WMT32VV" height=0 width=0
            style=display:none;visibility:hidden></iframe>
</noscript>
<noscript><img height=1 width=1 style="display:none"
               src="https://www.facebook.com/tr?id=278927249614169&ev=PageView&noscript=1"></noscript>
<noscript>
    <div><img src=https://mc.yandex.ru/watch/25611482 style="position:absolute; left:-9999px;" alt=""></div>
</noscript>

<div class="loaders">
    <div class="preloader">

    </div>
</div>

<div class="block1">
    <div class="header-ribbon" id="header-ribbon">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header-ribbon__inner">
                        <div>
                            <i class="play-icon"></i>
                            <p class="text">Прямой эфир начнется через</p>
                        </div>
                        <div>
                            <div class="timer">
                                <div class="timer__item">
                                    <span class="timer__item-title" data-hours="">00</span>
                                    <span class="timer__item-text">час</span>
                                </div>
                                <div class="timer__item">
                                    <span class="timer__item-title" data-minutes="">00</span>
                                    <span class="timer__item-text">мин</span>
                                </div>
                                <div class="timer__item">
                                    <span class="timer__item-title" data-seconds="">00</span>
                                    <span class="timer__item-text">сек</span>
                                </div>
                            </div>
                            <i class="arrow-icon"></i>
                            <a class="button-action" target="_blank"
                               href="https://static.yaroslav-samoylov.com/air/live?utm_source=lp_rk&utm_medium=plashka">Заходи</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bgr">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-xs-12 padding">
                    <p>Бесплатный онлайн - курс<br>
                        13-17 января 2020 (пн-вт-ср-чт-пт)<br>
                        в 20:00 по Мск/19:00 по Киеву. Участие онлайн из любой точки мира<br>
                    </p>
                    <p></p>
                    <h1 style="font-size: 60px">МУЖЧИНА:</h1>
                    <div class="desc">
                        <h1 class="text-left text-Regular"><b>честная</b> инструкция по применению.<br>Без драм и
                            манипуляций</h1>
                    </div>
                    <ul>
                        <li>• Как реанимировать «непонятные» отношения?</li>
                        <li>• Как привлекать достойных мужчин и выбрать одного?</li>
                        <li>• 1+1=11. Как создавать по-настоящему счастливые отношения?</li>
                        <li>• Умная или...? Королева без трона</li>
                        <li style="margin-bottom: 50px;">• Женщина: Хочу - Получаю</li>
                        <!-- <li>• Женщина на все 100!</li> -->
                    </ul>
                </div>

                <div id="elementId" class="col-md-4 col-xs-12 forma-1 ">
                    <h2 class="scroll" style="font-size: 1.5em;">ЗАПИШИТЕСЬ НА <br> <span style="color: #d22920;">БЕСПЛАТНЫЙ ОНЛАЙН</span>
                        <br> КУРС</h2>
                    <p style="margin-bottom: 5px;">И получите в подарок мини-книгу <br><span style="color: #d32a21;">«5 вопросов, чтобы определить,<br> Достойный ли мужчина перед Вами»</span>
                    </p>
                    <br>

                    <form class="form-a__form" method="POST" id="ajaxform"
                          onsubmit="r7k12.push({hit: 'metric',data:{metric:1}});return false;"
                          action="https://air2.yaroslav-samoylov.com/lead/zoho/">
                        <div class="form-row" style="line-height: 1.1857143;">
                            <label for="email">Email (пришлем ссылку на участие):</label>
                            <span class="hint" style="font-size: 12px;color: #999;">Например: test.2018@yandex.ru</span>
                            <input type="email" name="email" id="email" placeholder="Введите Ваш E-mail" val=""
                                   data-required="true" data-type="3">
                        </div>
                        <div class="form-row" style="line-height: 1.1857143;">
                            <label for="phone">Телефон (напомним по sms):</label>
                            <label for="form-a-phone" style="font-size: 12px;color: #999;">Телефон (напомним по
                                sms):</label>
                            <input type="tel" name="phone" id="phone" placeholder="+38 (ххх) ххx-хх-хх" val=""
                                   data-required="true">
                        </div>
                        <input type="hidden" name="id" value="<?= $product ?>">
                        <!--GEO-->
                        <input id="geo" name="geo" data-geo="true" type="hidden">
                        <input id="city" name="city" data-geo="true" type="hidden">
                        <!--GEO-->
                        <div class="form-row form-group privacy ">

                            <input id="form-a-privacy" data-button="submit-header" name="privacy" type="checkbox"
                                   class="privacy" aria-required="true" aria-invalid="false">
                            <label for="form-a-privacy"></label>
                            <span class="hint">
                      Я принимаю условия
                      <a href="//yaroslav-samoylov.com/publichnyj-dogovor-oferta.html"
                         target="_blank">публичной оферты</a>,
                      <a href="//yaroslav-samoylov.com/politika-konfidentsialnosti.html" target="_blank">политики конфиденциальности</a>,
                      <a href="//yaroslav-samoylov.com/garantiya-vozvrata.html" target="_blank">возврата</a>,
                      даю согласие на<a href="//yaroslav-samoylov.com/politika-konfidentsialnosti.html" target="_blank">
                          обработку своих персональных данных</a> и даю согласие на
                      <a href="//yaroslav-samoylov.com/soglasie-s-rassylkoj.html" target="_blank">рассылку</a>.
                       Принимать участие могут только дееспособные лица, достигшие совершеннолетия.
                  </span>


                            <!---->
                        </div>

                        <input type="button" value="Записаться" class="request-caller submit-header"/>
                        <p style="font-size: 10px;color: #999;line-height: 1;position: relative;top: 20px;">*Нажав
                            кнопку “записаться” клиент дает согласие на смс-информирование от сайта
                            yaroslav-samoylov.com и соглашается на обработку персональных данных, согласие на получение
                            рекламной рассылки и принимает Политику конфиденциальности.</p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block2">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="mac">
                    <iframe async defer width=auto height=auto
                            src="https://www.youtube.com/embed/0YyyUvioZuc?enablejsapi=1&amp;rel=0&amp;showinfo=0&amp;autoplay=0"
                            frameborder=0 allowfullscreen style="width=100% height=60; border-radius: 10px;"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bottom1">
    <div class="container cost">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btm click-menu-element button-ok"
                        onclick="ga('send', 'event', 'click', 'Хочу участвовать')">Хочу участвовать!<span
                            class="glyphicon glyphicon-ok"></span></button>
            </div>
        </div>
    </div>
</div>

<div class="block5">
    <div class="container">
        <div class="row">
            <div id="vtab">
                <ul>
                    <li class="home" style="background: none;">
                        <p style="font-size: 30px;font-family: ProximaNovaBold;left: -20px;position: relative;">1
                            БЛОК</p>
                        <div id="triangle-right"></div>
                    </li>
                    <li class="login" style="background: none;">
                        <p style="font-size: 30px;font-family: ProximaNovaBold;left: -20px;position: relative;">2
                            БЛОК</p>
                        <div id="triangle-right"></div>
                    </li>
                    <li class="support" style="background: none;">
                        <p style="font-size: 30px;font-family: ProximaNovaBold;left: -20px;position: relative;">3
                            БЛОК</p>
                        <div id="triangle-right"></div>
                    </li>
                    <li class="support" style="background: none;">
                        <p style="font-size: 30px;font-family: ProximaNovaBold;left: -20px;position: relative;">4
                            БЛОК</p>
                        <div id="triangle-right"></div>
                    </li>
                    <li class="support" style="background: none;">
                        <p style="font-size: 30px;font-family: ProximaNovaBold;left: -20px;position: relative;">5
                            БЛОК</p>
                        <div id="triangle-right"></div>
                    </li>
                </ul>
                <div>
                    <div class="thumbnail">
                        <div class="caption">
                            <div class="title">
                                <p>Сессия №1. 13 января 2020 (пн) 20:00 (по МСК), 19:00 (по Киеву)</p>
                                <h3 style="font-size: 23px;">КАК ПЕРЕЗАГРУЗИТЬ «НЕПОНЯТНЫЕ» ОТНОШЕНИЯ?</h3>
                            </div>
                            <ol>
                                <li>Можно ли перезагрузить “непонятные” отношения? Или лучше расстаться?</li>
                                <li>Как создать комфортные отношения в паре, которые устраивают обоих?</li>
                                <li>Как понять, насколько он честен и искренен в отношениях с вами?</li>
                                <li>Почему мужчина уходит из отношений без объяснений?</li>
                                <li>Как реагировать на неадекватное мужское поведение без ссор и истерик?</li>
                                <li>Почему мужчина держит вас на расстоянии и не предлагает замуж?</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="thumbnail">
                        <div class="caption">
                            <div class="title">
                                <p>Сессия №2. 14 января 2020 (вт) 20:00 (по МСК, 19:00 (по Киеву)</p>
                                <h3 style="font-size: 23px;">КАК ПРИВЛЕЧЬ ДОСТОЙНОГО МУЖЧИНУ ДЛЯ ОТНОШЕНИЙ?</h3>
                            </div>
                            <ol>
                                <li>Почему вы все время одна, несмотря на свою привлекательность?</li>
                                <li>Как легко познакомиться с достойным и сильным мужчиной?</li>
                                <li>Как заинтересовать мужчину после первого свидания?</li>
                                <li>Решение проблемы: “Я не нравлюсь тем, кто нравится мне”.</li>
                                <li>Как научиться правильно отказывать мужчинам?</li>
                                <li>Как говорить с мужчиной при встрече, чтобы его не отпугнуть?</li>
                                <li>Как выбрать отношения, в которых вы чувствуете себя женщиной?</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div>
                    <div class="thumbnail">
                        <div class="caption">
                            <div class="title">
                                <p>Сессия №3. 15 января 2020 (ср) 20:00 (по МСК, 19:00 (по Киеву)</p>
                                <h3 style="font-size: 23px;">КАК СОЗДАВАТЬ СЧАСТЛИВЫЕ ОТНОШЕНИЯ? ЧТОБЫ 1+1=11</h3>
                            </div>
                            <ol>
                                <li>Как создать гармоничные отношения и стать женщиной, которую искренне любят?</li>
                                <li>Как подтолкнуть мужчину к серьезным отношениям?</li>
                                <li>Как быть счастливой и наполненной в отношениях?</li>
                                <li>Как говорить с мужчиной, чтобы он хотел вас слышать?</li>
                                <li>Как перестать быть сильной в отношениях и довериться мужчине на все 100?</li>
                                <li>Как действовать, чтобы мужчина хотел радовать вас подарками и обеспечивать?</li>
                                <li>Простая формула: как стать для него лучшей женщиной, которую он когда-либо
                                    встречал.
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="thumbnail">
                        <div class="caption">
                            <div class="title">
                                <p>Сессия №4. 16 января 2020 (чт) 20:00 (по МСК), 19:00 (по Киеву)</p>
                                <h3 style="font-size: 23px;">УМНАЯ ИЛИ...? КОРОЛЕВА БЕЗ ТРОНА</h3>
                            </div>
                            <ol>
                                <li>Как мужчина выбирает женщину для отношений и замужества?</li>
                                <li>Как быстро понять, какой мужчина перед тобой?</li>
                                <li>Почему привлекаешь несерьезных и безответственных мужчин?</li>
                                <li>Что мешает тебе стать женщиной-удачей и привлекать достойных?</li>
                                <li>Почему мужчина не хочет брать на себя ответственность?</li>
                                <li>Как стать женщиной на все 100% и довериться мужчине?</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="thumbnail">
                        <div class="caption">
                            <div class="title">
                                <p>Сессия №5. 17 января 2020 (пт) 20:00 (по МСК), 19:00 (по Киеву)</p>
                                <h3 style="font-size: 23px;">ВАКЦИНА ОТ НИЩЕБРОДСТВА И ИЗМЕН В ОТНОШЕНИЯХ</h3>
                            </div>
                            <ol>
                                <li>Почему мужчины изменяют. Причины, о которых вы не догадывались.</li>
                                <li>Что делать, если он нашел любовницу.</li>
                                <li>Что такое нищебродство. Почему мужчина мало зарабатывает.</li>
                                <li>Любовный треугольник. Как выйти из него без потерь.</li>
                                <li>Женщина-удача. Как стать магнитом для успеха и благополучия.</li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container cost1">
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btm click-menu-element"
                        onclick="ga('send', 'event', 'click', 'принять участие')">принять участие<span
                            class="glyphicon glyphicon-ok"></span></button>
            </div>
        </div>
    </div>
</div>

<div class="with-form" style="margin-top: 35px;margin-bottom: 50px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row">
                    <div class="col-md-6">
                        <img style="left: -20px;position: relative;margin-bottom: 20px;" src="img/book.png"
                             alt="МУЖЧИНА: инструкция по применению, Книга">
                    </div>
                    <div class="col-md-6">
                        <h3 style="font-family: ProximaNovaRegular;font-size: 20px;text-align: left;">Запишитесь<br> на
                            бесплатный онлайн-курс<br> и получите мини-книгу в подарок</h3>
                        <form method="POST" class="form-a__form" id="ajaxform-1" onsubmit="return false;"
                              action="https://air2.yaroslav-samoylov.com/lead/zoho/">
                            <div class="form-row">
                                <label for="email-1">E-mail</label><br>
                                <p class="hint-p">(пришлем ссылку на участие в курсе):</p>
                                <span class="hint">Например: test.2018@yandex.ru</span>
                                <input type="email" id="email-1" name="email" placeholder="Введите Ваш E-mail"
                                       data-required="true" data-type="3">
                            </div>
                            <div class="form-row">
                                <label for="phone-1">Телефон:</label><br>
                                <p class="hint-p">(напомним о старте курса по sms):</p>
                                <label style="font-size: 12px;" class="hint" for="form-a-phone">Например: +38000000000,
                                    +7000000000</label>
                                <input type="tel" id="phone-1" class="phone" name="phone"
                                       placeholder="+38 (ххх) ххx-хх-хх" data-required="true">
                            </div>
                            <input type="hidden" name="id" value="<?= $product ?>">
                            <input id="geo-1" name="geo" data-geo="true" type="hidden">
                            <input id="city-1" name="city" data-geo="true" type="hidden">

                            <div class="form-row form-group privacy ">
                                <input id="form-a-privacy-footer" data-button="submit-footer"
                                       style=" width: 16px; height: auto;" name="privacy" type="checkbox"
                                       class="privacy" aria-required="true" aria-invalid="false">
                                <label for="form-a-privacy"></label>
                                <span class="hint">
                                  Я принимаю условия
                                  <a href="//yaroslav-samoylov.com/publichnyj-dogovor-oferta.html" target="_blank">публичной оферты</a>,
                                  <a href="//yaroslav-samoylov.com/politika-konfidentsialnosti.html" target="_blank">политики конфиденциальности</a>,
                                  <a href="//yaroslav-samoylov.com/garantiya-vozvrata.html" target="_blank">возврата</a>,
                                  даю согласие на<a href="//yaroslav-samoylov.com/politika-konfidentsialnosti.html"
                                                    target="_blank">
                                      обработку своих персональных данных</a> и даю согласие на
                                  <a href="//yaroslav-samoylov.com/soglasie-s-rassylkoj.html"
                                     target="_blank">рассылку</a>.
                                   Принимать участие могут только дееспособные лица, достигшие совершеннолетия.
                                </span>
                            </div>
                            <div class="form-row text-center">
                                <input type="button" value="Записаться" style="font-size: 20px;"
                                       class="request-caller submit-footer">
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block8">
    <div class="bgr">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-center-text" style="padding-top: 20px;">
                        <p style="padding: 0;margin: 0;">Россия:<span style="border: 0 solid #000 !important;">+7 (499) 322</span>
                            87 26</p>
                        <p style="padding: 0;margin: 0;">Украина:<span style="border: 0 solid #000 !important;">+38 (067) 329</span>
                            88 38</p>
                        <p style="padding: 0;margin: 0;">Казахстан: <span style="border: 0 solid #000 !important;">+88 (000) </span>80
                            49 77</p>
                    </div>
                    <!-- <a data-toggle="modal" data-target="#myModal" href="#" onclick="ga('send', 'event', 'click', 'Остались вопросы')">Остались вопросы – задайте их нам</a> -->
                    <p>© 2014-2018. Все права защищены. Любое копирование материалов разрешено только с согласия
                        правообладателей.<br>
                        По всем вопросам: info@yaroslav-samoylov.com</p>

                    <ul>
                        <li><a href="http://yaroslav-samoylov.com/politika-konfidentsialnosti.html" target="_blank">Политика
                                конфиденциальности</a></li>
                        <li><a href="http://yaroslav-samoylov.com/otkaz-ot-otvetstvennosti.html" target="_blank">Отказ
                                от ответственности</a></li>
                        <li><a href="http://yaroslav-samoylov.com/soglasie-s-rassylkoj.html" target="_blank">Согласие с
                                рассылкой</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>
<script async defer src="https://yaroslav-samoylov.com/script/auto-complementer.js?v=1.0.2"></script>
<script async defer src="https://yaroslav-samoylov.com/script/restorer.js?v=1.0.1"></script>
<script async defer src="https://yaroslav-samoylov.com/script/validation.js?v=1.0.1"></script>
<script async defer src="https://yaroslav-samoylov.com/script/cookie-functions.js?v=1.0.1"></script>
<script async defer src="https://yaroslav-samoylov.com/script/constants.js?v=1.0.1"></script>
<script src="js/main.js"></script>
<script src="js/google-sheets.js"></script>
<script async defer src="https://apis.google.com/js/api.js"
        onload="this.onload=function(){};handleClientLoad()"
        onreadystatechange="if (this.readyState === 'complete') this.onload()">
</script>
